<?php /** @noinspection ALL */ declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SomeAnonController extends AbstractController
{
    /**
     * @Route("/anon", name="app.anon")
     */
    public function __invoke(Request $request): Response
    {
        $b='';
        return new JsonResponse([__METHOD__]);
    }
}
